package com.xplorasoft.calculator;

public interface AnimatorListenerWrapper {
    void onAnimationStart();
}
